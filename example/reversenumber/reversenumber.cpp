#include <iostream>

using namespace std;

int main() {
    int number, reverse_number = 0;

    cin >> number;

    while (number > 0) {
        reverse_number *= 10;
        reverse_number += number % 10;
        number /= 10;
    }

    cout << reverse_number << endl;
    return 0;
}
