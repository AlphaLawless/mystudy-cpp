#include <iostream>

using namespace std;

int Screen_User() {
    int user;
    cout << "----Choice a User----" << endl;
    cout << "1. User 1 " << endl;
    cout << "2. User 2 " << endl;
    cin >> user;
    cout << '\n';
    return user;
}

double Operation(double balance) {
    int operation;
    do {
        cout << "--------MENU--------" << endl;
        cout << "1. Check balance" << endl;
        cout << "2. Deposit" << endl;
        cout << "3. Withdraw" << endl;
        cout << "4. Back to Screen User" << endl;
        cout << "5. Exit app" << endl;
        cout << "\n";
        cout << "What's the operation: ";
        cin >> operation;

        switch (operation) {
            case 1: cout << "Balance is U$ " << balance << endl;
                    break;
            case 2: cout << "How much do you want to deposit? \nU$ ";
                    double deposit;
                    cin >> deposit;
                    balance += deposit;
                    break;
            case 3: cout << "How much do you want to withdraw? \nU$ ";
                    double withdraw;
                    cin >> withdraw;
                    balance -= withdraw;
                    break;
            case 4: Screen_User();
            case 5: exit(1);
                    break;
        }
    } while (operation != 5);
    return balance;
}

void Screen_User1() {
    double balance = 500;
    Operation(balance);
}

void Screen_User2() {
    double balance = 300;
    Operation(balance);
}

int main() {
    int operation;
    int user;

    user = Screen_User();
    switch (user) {
        case 1: {
                    Screen_User1();
                    break;
                }
        case 2: {
                    Screen_User2();
                    break;
                }
    }
}
