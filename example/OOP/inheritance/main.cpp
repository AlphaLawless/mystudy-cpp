#include <iostream>
#include <list>
using namespace std;

class Person {
    // Encapsulation
    private:
        string Surname;
        int Age;
        list<string> hobbies;
    protected:
        string Name;

    public:
        // Use constructor in C++, calling the class
        Person(string name, string surname, int age) {
            Name = name;
            Surname = surname;
            Age = age;
        }

        void GetInfo() {
            cout << "Name is " << Name << endl;
            cout << "Surname is " << Surname << endl;
            cout << "Age is " << Age << endl;
            cout << "Your hobbies are: " << endl;
            for (string hobbie : hobbies ) {
                cout << "- "<< hobbie << endl;
            }

        }

        void HobbiesPushBack(string hobbie) {
            hobbies.push_back(hobbie);
        }
};

class Student: public Person {
    public:
        Student(string name, string surname, int age): Person(name, surname, age) {

        }
        void Degree() {
            string degree;
            cout << "What's your degree? ";
            cin >> degree;
            cout << Name << " is " << degree << endl;
        }
};

int main(void) {
    // Why use constructor ->
    Student person1("Alpha", "Melo", 21);
    person1.HobbiesPushBack("Learning computer science");
    person1.HobbiesPushBack("Study with your friends");
    // Using encapsulation creating a method within of the class Person
    person1.GetInfo();
    person1.Degree();
    cout << '\n';
    Person person2("Raimunda", "Sousa", 62);
    person2.GetInfo();
    person2.HobbiesPushBack("Like cookie.");
    person2.HobbiesPushBack("Lovely get out to see new places.");
}

