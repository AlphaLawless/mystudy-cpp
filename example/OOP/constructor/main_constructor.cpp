#include <iostream>
#include <list>
using namespace std;

class Person {
    public:
        string Name;
        string Surname;
        int Age;
        list<string> hobbies;

    // Use constructor in C++, call the class
    Person(string name, string surname, int age) {
        Name = name;
        Surname = surname;
        Age = age;
    }

    void GetInfo() {
        cout << "Name is " << Name << endl;
        cout << "Surname is " << Surname << endl;
        cout << "Age is " << Age << endl;
        cout << "The hobbies are: " << endl;
        for (string hobbie : hobbies ) {
            cout << "- "<< hobbie << endl;
        }
    }

};

int main(void) {
    // Why use constructor ->

    Person person1("Alpha", "Melo", 21);
    person1.hobbies.push_back("Learn programming language");
    person1.hobbies.push_back("Like Study");
    Person person2("Raimunda", "Sousa", 62);
    person2.hobbies.push_back("Like cookie");
    person2.hobbies.push_back("Go out to see new places");


    person1.GetInfo();
    person2.GetInfo();
}

