#include <iostream>
#include <list>
using namespace std;

class Person {
    public:
        string name;
        string surname;
        int age;        list<string> hobbies;
};

int main(void) {

    // Bad Code;
    Person person1;

    person1.name = "Alpha";
    person1.surname = "Melo";
    person1.age = 21;
    person1.hobbies = {"Learn programming lanague", "Play chess", "Learn math", "talk with people", "Like study"};

    cout << "Name is "<< person1.name << endl;
    cout << "Your surname is " << person1.surname << endl;
    cout << "Your age is " << person1.age << endl;
    cout << "Your hobbies are: " << endl;
    for(string hobbie : person1.hobbies ) {
        cout <<"- " << hobbie << endl;
    }

    Person person2;
    person2.name = "Raimunda";
    person2.surname = "Sousa";
    person2.age = 62;
    person2.hobbies = {"Cookie", "Have fun in shooping", "Go out to see new places"};

    cout << "Name is "<< person2.name << endl;
    cout << "Your surname is " << person2.surname << endl;
    cout << "Your age is " << person2.age << endl;
    cout << "Your hobbies are: " << endl;
    for(string hobbie : person2.hobbies ) {
        cout <<"- " << hobbie << endl;
    }

}
