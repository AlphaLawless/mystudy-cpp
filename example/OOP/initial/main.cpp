#include <iostream>
#include <list>
using namespace std;

class Person {
    public:
        string name;
        string surname;
        int age;
        list<string> hobbies;
};

int main(void) {
    Person person1;

    person1.name = "Alpha";
    person1.surname = "Melo";
    person1.age = 21;
    person1.hobbies = {"Learn programming lanague", "Play chess", "Learn math", "talk with people", "Like study"};

    cout << "Name is "<< person1.name << endl;
    cout << "Your surname is " << person1.surname << endl;
    cout << "Your age is " << person1.age << endl;
    cout << "Your hobbies are: " << endl;
    for(string hobbie : person1.hobbies ) {
        cout <<"- " << hobbie << endl;
    }
}
