#include <iostream>
using namespace std;

void insertion_sort(int arr[], int length)
{
    int i, key, j;
    for (i = 1; i < length; i++) {
        key = arr[i];

        j = i - 1;

        while(j >= 0 && arr[j] > key ) {
            arr[j + 1] = arr[j];
            j -= 1;
        }
        arr[j + 1] = key;
    }
}

int main(void)
{
    int arr[] = {3, 4, 5, 1, 2, 6};
    int length = sizeof(arr) / sizeof(arr[0]);

    insertion_sort(arr, length);

    for (int i = 0; i < length; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}
