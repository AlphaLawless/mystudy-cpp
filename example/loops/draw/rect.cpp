#include <iostream>
#include <iomanip>

using namespace std;

int main(void) {
    int width, height;
    char symbol;
    cout << "Enter with Width and Height: \n";
    cin >> width >> height;

    cout << "Enter with a symbol: \n";
    cin >> symbol;

    for (int h = 0; h < height; h++) {
        for (int w = 0; w < width; w++) {
            cout << setw(3) <<symbol;
        }
        cout << endl;
    }
}
