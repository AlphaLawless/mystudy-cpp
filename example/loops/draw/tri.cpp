#include <iostream>
#include <iomanip>

using namespace std;

int main(void) {
    int length;
    char symbol;
    cout << "Enter with a length: ";
    cin >> length;

    cout << "Enter with a symbol: ";
    cin >> symbol;

    for (int i = 1; i <= length;i++) {
        for (int j = 1; j <= i;j++) {
            cout << setw(2) << symbol;
        }
        cout << endl;
    }
}
