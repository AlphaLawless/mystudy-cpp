#include <iostream>

using namespace std;

int main(void) {
    int n;
    cin >> n;
    int arr[n];

    for (int i = 0; i < n; i++) {
        cin >> arr[i];
    }

    for (auto i : arr) {
        // We can added type in variable i too.
        cout << i << endl;
    }
}
