#include <iostream>

#define ll long long

using namespace std;

int main(void) {
    ll k = 10;

    for (int i = 1; i <= k; i++) {
        for (int j = 1; j <= k; j++) {
            cout << i << " x " << j << " = " << i * j << endl;
        }
    }
}
