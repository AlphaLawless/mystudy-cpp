#include <iostream>

using namespace std;

int main() {
    int age;
    cout << "How old are you? ";
    cin >> age;
    cout << "In 10 years you are going to be " << age + 10;

    return 0;
}
