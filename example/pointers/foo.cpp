#include <cstdlib>
#include <iostream>
using namespace std;

// Receber o input usando ponteiro, guardar o valor em uma lista, depois fazer um for para fazer a soma.

int main(void){
    int i=0, valor, soma=0;
    int arr[10];
    float media, multiplica;
    int * p1;
    p1 = (int *) malloc(sizeof(int));
    for(i=0;i<10;i++){
        cout << "Digite um valor " << endl;
        cin >> *p1;
        arr[i] = *p1;
    }
    i = 0;
    for (i = 0; i < 10; i++ ) {
        soma += arr[i];
    }
    media = (float)soma/i;
    cout << "A soma é " << soma << " e a media é " << media << endl;
}
