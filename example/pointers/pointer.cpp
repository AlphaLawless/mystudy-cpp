#include <iostream>
using namespace std;

int main(void) {
    int firstvalue = 5, secondvalue = 15;
    int * p1, * p2;

    p1 = &firstvalue;
    p2 = &secondvalue;

    *p1 = 10;
    *p2 = *p1;

    cout << firstvalue << endl;
    cout << *p2 << endl;
    cout << secondvalue << endl;
}
