#include <iostream>
using namespace std;

void print_number(int* ptr) {
    cout << *ptr << endl;
}

void print_letter(char* ptr) {
    cout << *ptr << endl;
}

void print(void* ptr, char type) {
    // handle type
    switch (type) {
        case 'i': cout << *((int*)ptr) << endl; break;
        case 'c': cout << *((char*)ptr) << endl; break;
    }
}

int main() {
    int number = 5;
    char letter = 'a';
    // print_number(&number);
    // print_letter(&letter);
    print(&number, 'i');
    print(&letter, 'c');
}
