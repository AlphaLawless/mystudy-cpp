#include <iostream>
using namespace std;

int main(void) {
    int firstvalue, secondvalue;
    int * mypointer;

    mypointer = &firstvalue;
    *mypointer = 10;
    mypointer = &secondvalue;
    *mypointer = 20;

    cout << "The first value is " << firstvalue << "\n";
    cout << "The second value is " << secondvalue << "\n";
}
