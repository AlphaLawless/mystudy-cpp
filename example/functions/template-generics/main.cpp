#include <iostream>

using namespace std;

template <class T>
T sum(T a, T b) {
  T result;
  result = a + b;
  return result;
}

int main(void) {
  int a = 2, b = 4, res1;
  double c = 1.5, d = 2.0, res2;

  res1 = sum<int>(a, b);
  res2 = sum<double>(c, d);

  cout << res1 << endl;
  cout << res2 << endl;
}
