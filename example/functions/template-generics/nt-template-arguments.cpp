#include <iostream>
using namespace std;

template <class T, int N>
T fixed_multiply(T val) {
    return val * N;
}

int main(void) {
    int N = 10;
    cout << fixed_multiply<int, 2>(N) << endl;
    cout << fixed_multiply<int, 3>(N) << endl;
}
