#include <iostream>
#include <string>
using namespace std;

int sum(int x, int y) { return x+y; }
string sum(string x, string y) { return x+y; }
double sum(double x, double y) { return x+y; }

int main(void) {
  int a, b, res;
  string a2, b2, res2;
  double a3, b3, res3;

  cout << "Enter with two numbers: ";
  cin >> a >> b;
  cout << sum(a,b) << endl;

  cout << "Enter with two numbers within of the ' ': ";
  cin >> a2 >> b2;
  cout << sum(a2, b2) << endl;

  cout << "Enter with two large numbers: ";
  cin >> a3 >> b3;
  cout << sum(a3,b3) << endl;
}
