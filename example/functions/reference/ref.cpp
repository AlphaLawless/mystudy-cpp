#include <iostream>

#define n '\n'

using namespace std;

void duplicate(int& a, int& b, int& c)
{
    a *= 2;
    b *= 2;
    c *= 2;
}

int main()
{
    int x=1, y=2, z=4;

    duplicate(x, y, z);
    cout << "x = " << x << n << "y = " << y << n << "z = " << z << n;
    return 0;
}
