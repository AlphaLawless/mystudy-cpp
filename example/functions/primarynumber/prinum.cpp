#include <iostream>
#include <string>
#include <algorithm>

#define n '\n'

using namespace std;

bool isPrimaryNumber(int number) {
    for (int i = 2; i < number;i++) {
        if (number % i == 0) {
            return false;
        }
    }

    return true;
}

int main(void) {
    string ans;


    do {
        int number;
        cout << "Enter with a number: ";
        cin >> number;

        bool isPrimaryFlag = isPrimaryNumber(number);

        if (isPrimaryFlag) {
            cout << "It is a primary number." << n;
        } else {
            cout << "It isn't a primary number." << n;
        }

        cout << "Do you want continue? Yes/No ";
        cin >> ans;

        for_each(ans.begin(), ans.end(), [](char & c) {
            c = tolower(c);
        });
    } while (ans == "yes");

}
