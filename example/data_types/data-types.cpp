#include <cstdint>
#include <iostream>

using namespace std;

int main() {
    int yearsOfBirth = 2000;
    float averageGrade = 4.5;
    double balance = 421441335;

    char gender = 'f';
    bool theDoorIsOpen = true;

    cout << "Size of int is " << sizeof(int) << " bytes\n";

    cout << "Int min value is " << INT64_MIN << endl;
    cout << "Int max value is " << INT64_MAX << endl;
    cout << "Size of unsigned is " << sizeof(unsigned int) << "bytes\n";
    cout << "UInt max value is " << UINT32_MAX << " bytes\n";
    cout << "Size of bool is " << sizeof(bool) << " bytes\n";
    cout << "Size of char is " << sizeof(char) << " bytes\n";
    cout << "Size of float is " << sizeof(float) << " bytes\n";
    cout << "Size of double is " << sizeof(double) << " bytes\n";

    return 0;
}
