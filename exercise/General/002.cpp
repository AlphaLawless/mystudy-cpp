/*
FATORIAL!
*/

#include <iostream>
using namespace std;

int main(void) {
    int fat = 1;
    int num = 5;
    for(int i = 1; i <= num; i++) {
        fat *= i;
    }

    cout << fat << endl;
}
