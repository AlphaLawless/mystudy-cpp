/*
Desafio pra quem estiver aprendendo lógica

Faça um programa que defina um método main em Java/C/C++ que lê Strings do teclado até que seja lida uma String igual a uma das quinze últimas digitadas.

Quando isto ocorre, o programa imprime o número de Strings lidas, o tamanho da maior String lida, e a concatenação de todas as Strings lidas exceto a última.

Use um array para armazenar as quinze últimas Strings lidas.

Use também métodos auxiliares para facilitar o entendimento do código.
*/

#include <bits/stdc++.h>
#include <cstring>

#define n '\n'

using namespace std;

int main(void) {
    string str[15];
    int key, i, length;

    length = sizeof(str)/ sizeof(str[0]);

    for(i = 0; i < length; i++) {
        cout << "digite a " << i + 1 << " ";
        cin >> str[i];

        if(str[0].size() > str[i].size()) {
            key = str[0].size();
        } else {
            str[0] = str[i];
            key = str[0].size();
        }
    }

    cout << "A o maior string: " << str[0] << n << "que teve tamanho de " << key << " caracteres" << n;

    cout << "foram lidas " << length << " strings" << n;

    cout << "A concatenação das strings juntas menos a última:" << n;
    for(i = 0; i < length - 1; i++){
        cout << str[i];
    }
    cout << endl;
}
