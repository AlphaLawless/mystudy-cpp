/* 1038 ---

Using the following table, write a program that reads a code and the amount of an item. After, print the value to pay. This is a very simple program with the only intention of practice of selection commands.

Input
The input file contains two integer numbers X and Y. X is the product code and Y is the quantity of this item according to the above table.

Output
The output must be a message "Total: R$ " followed by the total value to be paid, with 2 digits after the decimal point.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main(void)
{
  double code[6];

  code[0] = 0.00;
  code[1] = 4.00;
  code[2] = 4.50;
  code[3] = 5.00;
  code[4] = 2.00;
  code[5] = 1.50;

  int id, quantify;
  double ans;

  cin >> id >> quantify;

  ans = code[id] * (double)quantify;

  cout << fixed << setprecision(2);

  cout << "Total: R$ " << ans << endl;
}