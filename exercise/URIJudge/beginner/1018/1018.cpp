/* 1018 ---

In this problem you have to read an integer value and calculate the smallest possible number of banknotes in which the value may be decomposed. The possible banknotes are 100, 50, 20, 10, 5, 2 e 1. Print the read value and the list of banknotes.

Input
The input file contains an integer value N (0 < N < 1000000).

Output
Print the read number and the minimum quantity of each necessary banknotes in Portuguese language, as the given example. Do not forget to print the end of line after each line, otherwise you will receive “Presentation Error”.
*/

#include <iostream>

using namespace std;

int main()
{
    int value, hundred, fifty, twenty, ten, five, two, one;
    int fifty_mod, twenty_mod, ten_mod, five_mod, two_mod, one_mod;

    cin >> value;

    hundred = value / 100;
    fifty_mod = value % 100;
    fifty = fifty_mod / 50;
    twenty_mod = fifty_mod % 50;
    twenty = twenty_mod / 20;
    ten_mod = twenty_mod % 20;
    ten = ten_mod / 10;
    five_mod = ten_mod % 10;
    five = five_mod / 5;
    two_mod = five_mod % 5;
    two = two_mod / 2;
    one_mod = two_mod % 2;
    one = one_mod / 1;

    cout << value << endl;
    cout << hundred << " nota(s) de R$ 100,00" << endl;
    cout << fifty << " nota(s) de R$ 50,00" << endl;
    cout << twenty << " nota(s) de R$ 20,00" << endl;
    cout << ten << " nota(s) de R$ 10,00" << endl;
    cout << five << " nota(s) de R$ 5,00" << endl;
    cout << two << " nota(s) de R$ 2,00" << endl;
    cout << one << " nota(s) de R$ 1,00" << endl;

    return 0;
}
