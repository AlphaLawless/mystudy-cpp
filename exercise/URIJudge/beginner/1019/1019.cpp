/* 1019 ---

Read an integer value, which is the duration in seconds of a certain event in a factory, and inform it expressed in hours:minutes:seconds.

Input
The input file contains an integer N.

Output
Print the read time in the input file (seconds) converted in hours:minutes:seconds like the following example.
*/

#include <iostream>

using namespace std;

int main()
{
    int second = 0, minute = 0, hour = 0, second_initial;

    cin >> second_initial;

    hour = second_initial / 3600;
    minute = (second_initial - (hour * 3600)) / 60;
    second = (second_initial % 60);
    cout << hour << ":" << minute << ":" << second << endl;
    return 0;
}
