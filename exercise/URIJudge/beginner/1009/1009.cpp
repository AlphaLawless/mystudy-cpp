/* 1009 ---

In this problem, the task is to read a code of a product 1, the number of units of product 1, the price for one unit of product 1, the code of a product 2, the number of units of product 2 and the price for one unit of product 2. After this, calculate and show the amount to be paid.

Input
The input file contains two lines of data. In each line there will be 3 values: two integers and a floating value with 2 digits after the decimal point.

Output
The output file must be a message like the following example where "Valor a pagar" means Value to Pay. Remember the space after ":" and after "R$" symbol. The value must be presented with 2 digits after the point.
*/

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int p_code1, p_code2, unit1, unit2;
    double p_price1, p_price2, value;
    cout << fixed << setprecision(2);
    cin >> p_code1 >> unit1 >> p_price1;
    cin >> p_code2 >> unit2 >> p_price2;

    value = (unit1 * p_price1) + (unit2 * p_price2);
    cout << "VALOR A PAGAR: R$ " << value << endl;
    return 0;
}
