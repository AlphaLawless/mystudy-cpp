/* 1011 ---

Make a program that reads three floating point values: A, B and C. Then, calculate and show:
a) the area of the rectangled triangle that has base A and height C.
b) the area of the radius's circle C. (pi = 3.14159)
c) the area of the trapezium which has A and B by base, and C by height.
d) the area of ​​the square that has side B.
e) the area of the rectangle that has sides A and B.

Input
The input file contains three double values with one digit after the decimal point.

Output
The output file must contain 5 lines of data. Each line corresponds to one of the areas described above, always with a corresponding message (in Portuguese) and one space between the two points and the value. The value calculated must be presented with 3 digits after the decimal point
*/

#include <cmath>
#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

int main()
{
    float pointA,
        pointB,
        pointC,
        a_triangle,
        a_circle,
        a_trapezium,
        a_square,
        a_rectangle;
    cout << fixed << setprecision(3);
    cin >> pointA >> pointB >> pointC;

    a_triangle = (pointA * pointC) / 2;
    a_circle = 3.14159 * pow(pointC, 2);
    a_trapezium = ((pointA + pointB) / 2) * pointC;
    a_square = pow(pointB, 2);
    a_rectangle = (pointA * pointB);

    cout << "TRIANGULO: " << a_triangle << endl;
    cout << "CIRCULO: " << a_circle << endl;
    cout << "TRAPEZIO: " << a_trapezium << endl;
    cout << "QUADRADO: " << a_square << endl;
    cout << "RETANGULO: " << a_rectangle << endl;

    return 0;
}
