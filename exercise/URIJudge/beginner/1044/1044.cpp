/* 1044 --

Read two nteger values (A and B). After, the program should print the message "Sao Multiplos" (are multiples) or "Nao sao Multiplos" (aren’t multiples), corresponding to the read values.

Input
The input has two integer numbers.

Output
Print the relative message to the input as stated above.
*/

#include <iostream>
using namespace std;

int main(void)
{
    int a, b, k;
    cin >> a >> b;

    if (b > a)
    {
        int temp = b;
        b = a;
        a = temp;
    }
    k = a / b;
    if (k * b == a)
    {
        cout << "Sao Multiplos" << endl;
    }
    else
    {
        cout << "Nao sao Multiplos" << endl;
    }
}
