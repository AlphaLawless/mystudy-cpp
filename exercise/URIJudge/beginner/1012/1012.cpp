/* 1011 ---

Make a program that reads 3 integer values and present the greatest one followed by the message "eh o maior". Use the following formula:

MAIOR AB = (a + b + abs(a-b)) / 2

Input
The input file contains 3 integer values.

Output
Print the greatest of these three values followed by a space and the message “eh o maior”.
*/

#include <bits/stdc++.h>
#include <cmath>

using namespace std;

int main()
{
    int number1, number2, number3, greatAB, greatest;

    cin >> number1 >> number2 >> number3;

    greatAB = (number1 + number2 + abs(number1 - number2)) / 2;
    greatest = (greatAB + number3 + abs(greatAB - number3)) / 2;
    cout << greatest << " eh o maior" << endl;
    return 0;
}
