/* 1042 ---

Read three integers and sort them in ascending order. After, print these values in ascending order, a blank line and then the values in the sequence as they were readed.

Input
The input contains three integer numbers.

Output
Present the output as requested above.
*/

#include <iostream>
using namespace std;

int main(void)
{
    int a, b, c, a1, b1, c1;
    cin >> a >> b >> c;
    a1 = a;
    b1 = b;
    c1 = c;

    if (b > c)
    {
        int temp = b;
        b = c;
        c = temp;
    }
    if (a > b)
    {
        int temp = a;
        a = b;
        b = temp;
    }
    if (b > c)
    {
        int temp = b;
        b = c;
        c = temp;
    }
    cout << a << endl;
    cout << b << endl;
    cout << c << endl;
    cout << endl;
    cout << a1 << endl;
    cout << b1 << endl;
    cout << c1 << endl;
}
