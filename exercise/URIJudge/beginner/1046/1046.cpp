/* 1046 ---

Read the start time and end time of a game, in hours. Then calculate the duration of the game, knowing that the game can begin in a day and finish in another day, with a maximum duration of 24 hours. The message must be printed in portuguese “O JOGO DUROU X HORA(S)” that means “THE GAME LASTED X HOUR(S)”

Input
Two integer numbers representing the start and end time of a game.

Output
Print the duration of the game as in the sample output.
*/

#include <iostream>
using namespace std;

int main(void)
{
    int x, y;
    cin >> x >> y;
    int ans = 0;

    if (x == 0)
    {
        x = 24;
    }
    if (y == 0)
    {
        y = 24;
    }
    if (x == y)
    {
        y -= 24;
        while (x > y)
        {
            ans += 1;
            x--;
        }
        cout << "O JOGO DUROU " << ans << " HORA(S)" << endl;
        ;
    }
    if (x > y)
    {
        while (x < 24)
        {
            ans += 1;
            x++;
        }
        cout << "O JOGO DUROU " << ans + y << " HORA(S)" << endl;
    }

    if (x < y)
    {
        while (x < y)
        {
            ans += 1;
            x++;
        }
        cout << "O JOGO DUROU " << ans << " HORA(S)" << endl;
    }
}
