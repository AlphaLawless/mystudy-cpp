/* 1021 ---

Read a value of floating point with two decimal places. This represents a monetary value. After this, calculate the smallest possible number of notes and coins on which the value can be decomposed. The considered notes are of 100, 50, 20, 10, 5, 2. The possible coins are of 1, 0.50, 0.25, 0.10, 0.05 and 0.01. Print the message “NOTAS:” followed by the list of notes and the message “MOEDAS:” followed by the list of coins.

Input
The input file contains a value of floating point N (0 ≤ N ≤ 1000000.00).

Output
Print the minimum quantity of banknotes and coins necessary to change the initial value, as the given example.
*/

#include <iostream>

using namespace std;

int main(void)
{
    int value_1, hundred, fifty, twenty, ten, five, two, one;
    int cent, fifty_cent, twenty_cent, ten_cent, five_cent, two_cent, one_cent;

    double value;

    cin >> value;
    value_1 = (int)value;
    value -= value_1;

    hundred = value_1 / 100;
    fifty = (value_1 % 100) / 50;
    twenty = (value_1 % 50) / 20;
    ten = ((value_1 % 50) % 20) / 10;
    five = (((value_1 % 50) % 20) % 10) / 5;
    two = ((((value_1 % 50) % 20) % 10) % 5) / 2;
    one = (((((value_1 % 50) % 20) % 10) % 5) % 2) / 1;

    cent = (value * 100);

    fifty_cent = cent / 50;
    twenty_cent = (cent % 50) / 25;
    ten_cent = ((cent % 50) % 25) / 10;
    five_cent = (((cent % 50) % 25) % 10) / 5;
    one_cent = ((((cent % 50) % 25) % 10) % 5) / 1;

    cout << "NOTAS:\n";
    cout << hundred << " nota(s) de R$ 100.00" << endl;
    cout << fifty << " nota(s) de R$ 50.00" << endl;
    cout << twenty << " nota(s) de R$ 20.00" << endl;
    cout << ten << " nota(s) de R$ 10.00" << endl;
    cout << five << " nota(s) de R$ 5.00" << endl;
    cout << two << " nota(s) de R$ 2.00" << endl;
    cout << "MOEDAS:\n";
    cout << one << " moeda(s) de R$ 1.00" << endl;
    cout << fifty_cent << " moeda(s) de R$ 0.50" << endl;
    cout << twenty_cent << " moeda(s) de R$ 0.25" << endl;
    cout << ten_cent << " moeda(s) de R$ 0.10" << endl;
    cout << five_cent << " moeda(s) de R$ 0.05" << endl;
    cout << one_cent << " moeda(s) de R$ 0.01" << endl;
}
