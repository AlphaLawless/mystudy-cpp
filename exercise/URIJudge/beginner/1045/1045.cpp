/* 145 ---

Read 3 double numbers (A, B and C) representing the sides of a triangle and arrange them in decreasing order, so that the side A is the biggest of the three sides. Next, determine the type of triangle that they can make, based on the following cases always writing an appropriate message:
if A ≥ B + C, write the message: NAO FORMA TRIANGULO
if A2 = B2 + C2, write the message: TRIANGULO RETANGULO
if A2 > B2 + C2, write the message: TRIANGULO OBTUSANGULO
if A2 < B2 + C2, write the message: TRIANGULO ACUTANGULO
if the three sides are the same size, write the message: TRIANGULO EQUILATERO
if only two sides are the same and the third one is different, write the message: TRIANGULO ISOSCELES
Input
The input contains three double numbers, A (0 < A) , B (0 < B) and C (0 < C).

Output
Print all the classifications of the triangle presented in the input.
*/

#include <iostream>
using namespace std;

int main(void)
{
    double a, b, c;
    cin >> a >> b >> c;

    if (b < c)
    {
        double temp = c;
        c = b;
        b = temp;
    }
    if (a < b)
    {
        double temp = b;
        b = a;
        a = temp;
    }
    if (b < c)
    {
        double temp = b;
        b = c;
        c = temp;
    }
    if (a >= (b + c))
    {
        cout << "NAO FORMA TRIANGULO" << endl;
    }
    else
    {
        if (a * a == (b * b) + (c * c))
        {
            cout << "TRIANGULO RETANGULO" << endl;
        }
        if (a * a > (b * b) + (c * c))
        {
            cout << "TRIANGULO OBTUSANGULO" << endl;
        }
        if (a * a < (b * b) + (c * c))
        {
            cout << "TRIANGULO ACUTANGULO" << endl;
        }
        if (a == b && a == c)
        {
            cout << "TRIANGULO EQUILATERO" << endl;
        }
        if ((a == b || b == c || a == c) && (a != b || a != c || b != c))
        {
            cout << "TRIANGULO ISOSCELES" << endl;
        }
    }
}
