/* 1036 ---

Read 3 floating-point numbers. After, print the roots of bhaskara’s formula. If it's impossible to calculate the roots because a division by zero or a square root of a negative number, presents the message “Impossivel calcular”.

Input
Read 3 floating-point numbers (double) A, B and C.

Output
Print the result with 5 digits after the decimal point or the message if it is impossible to calculate.
*/

#include <iostream>
#include <iomanip>
#include <cmath>

#define d double

using namespace std;

int main()
{
    d a, b, c, delta, r1, r2;
    cout << fixed << setprecision(5);
    cin >> a >> b >> c;

    delta = pow(b, 2) - (4 * a * c);

    if (delta < 0 || a <= 0)
    {
        cout << "Impossivel calcular" << endl;
    }
    else
    {
        r1 = (-b + sqrt(delta)) / (2 * a);
        r2 = (-b - sqrt(delta)) / (2 * a);

        cout << "R1 = " << r1 << endl;
        cout << "R2 = " << r2 << endl;
    }
}
