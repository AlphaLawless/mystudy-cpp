/* 1043 ---

Read three point floating values (A, B and C) and verify if is possible to make a triangle with them. If it is possible, calculate the perimeter of the triangle and print the message:


Perimetro = XX.X


If it is not possible, calculate the area of the trapezium which basis A and B and C as height, and print the message:


Area = XX.X

Input
The input file has tree floating point numbers.

Output
Print the result with one digit after the decimal point.
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main(void)
{
    float a, b, c;
    cin >> a >> b >> c;
    cout << fixed << setprecision(1);

    float ans;
    if ((a + b) > c && (a + c) > b && (b + c) > a)
    {
        ans = a + b + c;
        cout << "Perimetro = " << ans << endl;
    }
    else
    {
        ans = ((a + b) * c) / 2;
        cout << "Area = " << ans << endl;
    }
}
